import smtplib
import telebot
from telebot import types
import MySQLdb
import time

TOKEN = 'Token'
db = MySQLdb.connect(host="localhost", user="username", passwd="password", db="database name")
bot_name = "username of your bot"
TABLE = "Tablename" #table should have 2 rows: chatid (int) and stage (int)

tb = telebot.TeleBot(TOKEN)
cursor = db.cursor()

global sender
global receivers
global subject
global mailtext
global fromperson
global toperson

def listener(messages):
    """
    When new messages arrive TeleBot will call this function.
    """
    for m in messages:
        chat_id = m.chat.id
        if m.content_type == 'text' and m.text != "/start" and m.text != "/mail" and m.text !="/start@"+str(bot_name) and m.text!= "/mail@"+str(bot_name):
            text = m.text
            search = cursor.execute('SELECT stage FROM '+str(TABLE)+' WHERE chatid = "'+str(chat_id)+'"')
            force_reply = types.ForceReply()
            if search == 0:
                 tb.send_message(chat_id, "Please use the /start command first so I can get to know you better.")
                 
            else:
                 stage1 = cursor.fetchone()
                 stage = stage1[0]
                 #if stage == 0:
                 #tb.send_message(chat_id, "If you would like to send a mail please use /mail")
                 if stage == 1:
                     global sender 
                     sender = m.text
                     cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "2" where chatid = "'+str(chat_id)+'"')
                     db.commit()
                     tb.send_message(chat_id, "I now need the senders name.", reply_markup = force_reply)
                 if stage == 2:
                     global fromperson 
                     fromperson = m.text
                     cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "3" where chatid = "'+str(chat_id)+'"')
                     db.commit()
                     tb.send_message(chat_id, "I now need the receivers mail address.", reply_markup = force_reply)
                 if stage == 3:
                     global receivers 
                     receivers = m.text
                     cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "4" where chatid = "'+str(chat_id)+'"')
                     db.commit()
                     tb.send_message(chat_id, "I now need the receivers name.", reply_markup = force_reply)
                 if stage == 4:
                     global toperson 
                     toperson = m.text
                     cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "5" where chatid = "'+str(chat_id)+'"')
                     db.commit()
                     tb.send_message(chat_id, "I now need the subject.", reply_markup = force_reply)
                 if stage == 5:
                     global subject
                     subject = m.text
                     cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "6" where chatid = "'+str(chat_id)+'"')
                     db.commit()
                     tb.send_message(chat_id, "I now need the mail text.", reply_markup = force_reply)
                 if stage == 6:
                     global mailtext
                     mailtext = m.text
                     sendmail(sender, fromperson, receivers, toperson, subject, mailtext)
                     tb.send_message(chat_id,"Sending...")
                     cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "0" where chatid = "'+str(chat_id)+'"')
                     db.commit()
                     
                 

tb.set_update_listener(listener) #register listener

@tb.message_handler(commands=['start'])
def command_start(message):
    chat_id = message.chat.id
    search = cursor.execute('SELECT stage FROM '+str(TABLE)+' WHERE chatid = "'+str(chat_id)+'"')
    if search == 0:
         cursor.execute('INSERT into '+str(TABLE)+' (chatid, stage) values ('+str(chat_id)+', 0)')
         print("new user. Inserting stage 0")
         tb.send_message(chat_id, "Hello you are now my friend!")
         db.commit()
    else:
         stage1 = cursor.fetchone()
         stage = stage1[0]
         tb.send_message(chat_id,"I already know you ;)")
         
@tb.message_handler(commands=['mail'])
def command_echo(message):
      chat_id = message.chat.id
      force_reply = types.ForceReply()
      tb.send_message(chat_id, "First we need the sender mail address.", reply_markup = force_reply)
      cursor.execute('UPDATE '+ str(TABLE)+ ' SET stage = "1" where chatid = "'+str(chat_id)+'"')
      db.commit()
      
def sendmail(sender, fromperson, receivers, toperson, subject, mailtext):
     receive = []
     receive.append(str(receivers))
     message = """From: """+str(fromperson)+""" <"""+str(sender)+""">
To: """+str(toperson)+""" <"""+str(receivers)+""">
Subject: """+str(subject)+"""

"""+str(mailtext)
     try:
         print(message)
         smtpObj = smtplib.SMTP('localhost')
         smtpObj.sendmail(sender, receive, message)         
         print "Successfully sent email"
     except SMTPException:
         print "Error: unable to send email"
            

tb.polling(none_stop=True)

while True: # Don't let the main Thread end.
    time.sleep(300) #reduce high cpu load of pass

